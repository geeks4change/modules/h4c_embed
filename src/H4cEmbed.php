<?php

namespace Drupal\h4c_embed;

use Drupal\link\LinkItemInterface;
use Drupal\node\NodeInterface;

class H4cEmbed {

  /**
   * Check if the current page is a h4c_embed node, and if yes, return it.
   *
   * @return \Drupal\node\NodeInterface|null
   *   The embed node.
   */
  public static function getEmbedNode(): ?NodeInterface {
    if (
      \Drupal::routeMatch()->getRouteName() === 'entity.node.canonical'
      && ($node = \Drupal::routeMatch()->getParameter('node'))
      && $node instanceof NodeInterface
      && $node->bundle() === 'h4c_embed'
    ) {
      return $node;
    }
    else {
      return NULL;
    }
  }

  /**
   * Check if the current page is a h4c_embed node AND url has '?embed', and if yes, return it.
   *
   * @return \Drupal\node\NodeInterface|null
   *   The embed node.
   */
  public static function getEmbeddedEmbedNode(): ?NodeInterface {
    return \Drupal::request()->query->has('embed') ?
      self::getEmbedNode() : NULL;
  }

  /**
   * For a (embed) node, return its embed domains.
   *
   * @param \Drupal\node\NodeInterface $node
   *
   * @return array
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public static function embedUrls(NodeInterface $node): array {
    if ($node->hasField('field_h4c_embed_domains')) {
      $urls = $node->get('field_h4c_embed_domains');
      $urlStrings = [];
      foreach ($urls as $url) {
        assert($url instanceof LinkItemInterface);
        $urlString = $url->get('uri')->getValue();
        $urlStrings[] = $urlString;
      }
      return $urlStrings;
    }
    return [];
  }
}
