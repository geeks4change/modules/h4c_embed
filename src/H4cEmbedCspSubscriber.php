<?php

namespace Drupal\h4c_embed;

use Drupal\csp\CspEvents;
use Drupal\csp\Event\PolicyAlterEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class H4cEmbedCspSubscriber implements EventSubscriberInterface {

  public static function getSubscribedEvents() {
    $events[CspEvents::POLICY_ALTER] = ['onCspPolicyAlter', -999];
    return $events;
  }

  public function onCspPolicyAlter(PolicyAlterEvent $alterEvent) {
    $policy = $alterEvent->getPolicy();
    $response = $alterEvent->getResponse();

    $node = H4cEmbed::getEmbedNode();
    if ($node) {
      $urls = H4cEmbed::embedUrls($node);
      array_unshift($urls, "'self'");

      // This is called twice, for $policy->isReportOnly() = true / false.
      // Set both variants to the same setting.
      $policy->setDirective('frame-ancestors', $urls);
    }
  }

}
