# H4C Embed

Provides a layoutable content type to embed in an iframe on selectable domains.

## Beware on wodby!

Wodby's nginx writes a csp header that contradicts and voids ours.
See NGINX_HEADERS_CONTENT_SECURITY_POLICY in https://github.com/wodby/nginx.

## Status

Currently any links, also form submit buttons are redirected to `target=_blank`

Find more infos in: https://gitlab.com/geeks4change/sites/site-h4c-multi/-/issues/17

## Example embed

```
<script src="https://cdn.jsdelivr.net/gh/davidjbradshaw/iframe-resizer@v4.2.10/js/iframeResizer.min.js" type="text/javascript"></script>
<script>
document.addEventListener("DOMContentLoaded", function(){
	iFrameResize({log:false});
});
</script>
<iframe src="https://foo.bar/de/node/223?embed" frameborder="0" style="width:1px;min-width:100%"></iframe>
```
